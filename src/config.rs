use serde::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    pub api_key: String,
    #[serde(default = "Tracker::default")]
    pub tracker: Tracker,
    #[serde(default = "OutputStyle::default")]
    pub output_style: OutputStyle,
    #[serde(default = "CollageType::default")]
    pub collage_type: CollageType,
    #[serde(default = "default_threshold")]
    pub threshold: u32,
    pub json_output: Option<String>,
}

#[derive(Deserialize)]
pub enum Tracker {
    #[serde(alias = "red")]
    #[serde(alias = "redacted")]
    RED,
    #[serde(alias = "ops")]
    #[serde(alias = "orpheus")]
    OPS,
}

impl Tracker {
    pub fn url(&self) -> &str {
        match *self {
            Tracker::RED => "https://redacted.sh/",
            Tracker::OPS => "https://orpheus.network/",
        }
    }
    fn default() -> Self {
        Tracker::RED
    }
}

#[derive(Deserialize)]
pub enum OutputStyle {
    #[serde(alias = "pretty")]
    Pretty,
    #[serde(alias = "csv")]
    CSV,
}
impl OutputStyle {
    fn default() -> Self {
        OutputStyle::Pretty
    }
}

#[derive(Deserialize)]
pub enum CollageType {
    #[serde(alias = "all")]
    All,
    #[serde(alias = "personal")]
    Personal,
    #[serde(alias = "nonpersonal")]
    NonPersonal,
}
impl CollageType {
    fn default() -> Self {
        CollageType::All
    }
}

fn default_threshold() -> u32 {
    0
}
