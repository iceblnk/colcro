use crate::collage::Collage;
use crate::torrent_group::TorrentGroup;
use serde::{Deserialize, Serialize};
use std::rc::Rc;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Match {
    pub collage: Collage,
    pub shared: Vec<Rc<TorrentGroup>>,
}

impl Match {
    pub fn new(collage: Collage, shared: Vec<Rc<TorrentGroup>>) -> Self {
        Match { collage, shared }
    }
}
