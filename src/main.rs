use colcro::collage::{self, Collage};
use colcro::config;
use colcro::matches::Match;
use colcro::torrent_group;
use reqwest::{header, Method, Request, Url};
use serde_yaml;
use std::collections::HashMap;
use std::env;
use std::error::Error;
use std::rc::Rc;
use std::time::Duration;
use tower::{Service, ServiceExt};

#[macro_use]
extern crate prettytable;
use prettytable::Table;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let cfg = read_cfg()?;

    let collage_id = read_args();

    let client = get_request_client(&cfg.api_key)?;

    let mut svc = tower::ServiceBuilder::new()
        .rate_limit(10, Duration::new(10, 0))
        .service(tower::service_fn(move |req| client.execute(req)));

    let collage = get_primary_collage_data(collage_id, &mut svc, &cfg).await?;

    let group_ids = collage
        .torrent_group_ids
        .ok_or("unable to get torrent group ids from collage response")?;

    let mut matches: Vec<Match> = count_cross_collages(group_ids, &mut svc, &cfg).await?;
    matches.sort_by(|a, b| (&b.shared.len()).cmp(&a.shared.len()));

    print_results(&matches, &cfg)?;

    write_json(&matches, &cfg)?;

    Ok(())
}

fn read_cfg() -> Result<config::Config, Box<dyn Error>> {
    let file = std::fs::File::open("colcro.yaml");
    match file {
        Err(_) => Err("could not find config file colcro.yaml in current directory".into()),
        Ok(f) => Ok(serde_yaml::from_reader(f)?),
    }
}

fn read_args() -> u32 {
    let args: Vec<String> = env::args().collect();
    args.get(1)
        .unwrap_or(&String::from("18617"))
        .parse::<u32>()
        .unwrap_or(18617)
}

fn get_request_client(api_key: &String) -> Result<reqwest::Client, Box<dyn Error>> {
    let mut headers = header::HeaderMap::new();
    let mut auth_value = header::HeaderValue::from_str(api_key)?;
    auth_value.set_sensitive(true);
    headers.insert(header::AUTHORIZATION, auth_value);

    Ok(reqwest::Client::builder()
        .default_headers(headers)
        .build()?)
}

async fn get_primary_collage_data<S>(
    id: u32,
    svc: &mut tower::limit::RateLimit<S>,
    cfg: &config::Config,
) -> Result<Collage, Box<dyn Error>>
where
    S: tower::Service<Request, Error = reqwest::Error, Response = reqwest::Response>,
{
    let req = Request::new(
        Method::GET,
        Url::parse(&format!(
            "{}ajax.php?action=collage&id={}&showonlygroups",
            &cfg.tracker.url(),
            id,
        ))?,
    );

    let res: collage::ApiResponse = svc.ready().await?.call(req).await?.json().await?;

    match res.error {
        Some(e) => Err(e.into()),
        None => res
            .response
            .ok_or(String::from("unable to parse collage response").into()),
    }
}

async fn count_cross_collages<S>(
    ids: Vec<u32>,
    svc: &mut tower::limit::RateLimit<S>,
    cfg: &config::Config,
) -> Result<Vec<Match>, Box<dyn Error>>
where
    S: tower::Service<Request, Error = reqwest::Error, Response = reqwest::Response>,
{
    let mut matches: HashMap<u32, Match> = HashMap::new();

    for id in ids {
        let req = Request::new(
            Method::GET,
            Url::parse(&format!(
                "{}ajax.php?action=torrentgroup&id={}",
                &cfg.tracker.url(),
                id
            ))?,
        );
        let res: torrent_group::ApiResponse = svc.ready().await?.call(req).await?.json().await?;

        let group = res.response.clone().ok_or_else(|| {
            res.error
                .unwrap_or(String::from("unable to parse torrent group response"))
        })?;
        let group_ref = Rc::new(group);
        let collages = match &cfg.collage_type {
            config::CollageType::All => Box::new(
                group_ref
                    .collages
                    .clone()
                    .unwrap_or(vec![])
                    .into_iter()
                    .chain(
                        group_ref
                            .personal_collages
                            .clone()
                            .unwrap_or(vec![])
                            .into_iter(),
                    ),
            )
                as Box<dyn std::iter::Iterator<Item = Collage>>,
            config::CollageType::Personal => Box::new(
                group_ref
                    .personal_collages
                    .clone()
                    .unwrap_or(vec![])
                    .into_iter(),
            )
                as Box<dyn std::iter::Iterator<Item = Collage>>,
            config::CollageType::NonPersonal => {
                Box::new(group_ref.collages.clone().unwrap_or(vec![]).into_iter())
                    as Box<dyn std::iter::Iterator<Item = Collage>>
            }
        };
        for collage in collages {
            let mtch = matches
                .entry(collage.id)
                .or_insert(Match::new(collage, vec![]));
            mtch.shared.push(group_ref.clone());
        }
    }
    Ok(matches.values().cloned().collect())
}

fn print_results(res: &Vec<Match>, cfg: &config::Config) -> Result<(), Box<dyn Error>> {
    let mut table = Table::new();

    for mtch in res {
        let num_shared = mtch.shared.len() as u32;
        if num_shared > cfg.threshold {
            table.add_row(row![
                mtch.collage.name,
                format!("{}collages.php?id={}", cfg.tracker.url(), mtch.collage.id),
                num_shared
            ]);
        }
    }

    if let config::OutputStyle::Pretty = cfg.output_style {
        table.printstd();
        Ok(())
    } else {
        table.to_csv(std::io::stdout())?;
        Ok(())
    }
}

fn write_json(matches: &Vec<Match>, cfg: &config::Config) -> Result<(), Box<dyn Error>> {
    if let Some(f) = cfg.json_output.clone() {
        let file = std::fs::File::create(f)?;
        serde_json::to_writer(file, &matches)?;
    };
    Ok(())
}
