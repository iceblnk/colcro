use crate::collage;
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Debug)]
pub struct ApiResponse {
    pub status: String,
    #[serde(deserialize_with = "parse_nested_group")]
    pub response: Option<TorrentGroup>,
    pub error: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TorrentGroup {
    pub id: u32,
    pub name: String,
    pub year: u32,
    #[serde(rename = "musicInfo")]
    pub music_info: MusicInfo,
    #[serde(skip_serializing)]
    pub collages: Option<Vec<collage::Collage>>,
    #[serde(rename = "personalCollages")]
    #[serde(skip_serializing)]
    pub personal_collages: Option<Vec<collage::Collage>>,
}

//#[derive(Serialize, Deserialize, Debug, Clone)]
//pub struct TorrentGroupMinimal {
//    pub id: u32,
//    pub name: String,
//    pub year: u32,
//    #[serde(rename = "musicInfo")]
//    pub music_info: MusicInfo,
//}
//
//impl From<TorrentGroup> for TorrentGroupMinimal {
//    fn from(item: TorrentGroup) -> Self {
//        TorrentGroupMinimal {
//            id: item.id,
//            name: item.name,
//            year: item.year,
//            music_info: item.music_info,
//        }
//    }
//}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MusicInfo {
    pub composers: Vec<Artist>,
    pub dj: Vec<Artist>,
    pub artists: Vec<Artist>,
    pub with: Vec<Artist>,
    pub conductor: Vec<Artist>,
    #[serde(rename = "remixedBy")]
    pub remixed_by: Vec<Artist>,
    pub producer: Vec<Artist>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Artist {
    id: u32,
    name: String,
}

fn parse_nested_group<'de, D>(deserializer: D) -> Result<Option<TorrentGroup>, D::Error>
where
    D: serde::Deserializer<'de>,
{
    #[derive(Deserialize)]
    struct GroupResponse {
        group: Option<TorrentGroup>,
    }
    GroupResponse::deserialize(deserializer).map(|r| r.group)
}
