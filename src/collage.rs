use serde::{Deserialize, Serialize};

#[derive(Deserialize, Debug)]
pub struct ApiResponse {
    pub status: String,
    pub response: Option<Collage>,
    pub error: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Collage {
    pub id: u32,
    pub name: String,
    #[serde(rename = "torrentGroupIDList")]
    #[serde(skip_serializing)]
    pub torrent_group_ids: Option<Vec<u32>>,
    //pub description: Option<String>,
    //#[serde(rename = "subscriberCount")]
    //pub subscriber_count: Option<u32>,
}

impl Eq for Collage {}

impl PartialEq for Collage {
    fn eq(&self, other: &Collage) -> bool {
        self.id == other.id
    }
}

impl std::hash::Hash for Collage {
    fn hash<H>(&self, state: &mut H)
    where
        H: std::hash::Hasher,
    {
        state.write_i32(self.id as i32);
        let _ = state.finish();
    }
}
