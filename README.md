![](./colcro.png)

# collage crossing

View collages that share albums with a target collage.

## set up

1. get an API key
2. edit `colcro.yaml` to include your API key
3. adjust settings as you like

## usage

`$ ./colcro <collage ID>`

example:
`$ ./colcro 21388`

## available settings

All settings are optional except for `api_key`.

* `api_key`: must be a valid API key
* `tracker`: RED or OPS. only tested on RED. default: `red`.
* `output_style`: `pretty` or `csv`. default: `pretty`.
  * `pretty`: prints a pretty table view. may not appear correctly on narrow windows.
  * `csv`: prints csv.
* `collage_type`: `all`, `personal`, or `nonpersonal`. default: `all`.
  * `all`: all types of collages included in the output.
  * `personal`: only personal collages are included in the output.
  * `nonpersonal`: personal collages will not be included in the output.
* `threshold`: only collages with more than `threshold` shared albums will be included in the output. default: `0`.
* `json_output`: name of the file to output all collected data to JSON.
